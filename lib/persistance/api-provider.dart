import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:rule_viewer_40k/models/book.dart';
import 'package:rule_viewer_40k/models/index.dart';
import 'package:rule_viewer_40k/models/data-structure.dart';

class ApiProvider {

  final String _baseUrl;
  ApiProvider(this._baseUrl);

  final Dio _dio = Dio();


  Future <Index> fetchIndex() async {

    String url = _baseUrl + "/blob/main/index.json?raw=true";

    try {

      Response response = await _dio.get(url);
      print(response.data);
      return Index.fromJson(json.decode(response.data));

    } catch (error, stacktrace) {

      print("Exception occured: $error stackTrace: $stacktrace");
      return Index.withError("$error");

    }
  }

  Future <Book> fetchBook(String slug) async {

    String url = _baseUrl + "/blob/main/" + slug + "?raw=true";

    try {

      Response response = await _dio.get(url,);
      return Book.fromJson(json.decode(response.data));

    } catch (error, stacktrace) {

      print("Exception occured: $error stackTrace: $stacktrace");
      return Book.withError("$error");

    }
  }


  Future<DataStructure> fetchData() async {

    Index index = await fetchIndex();

    DataStructure dataStructure = new DataStructure();

    if (index.files.length > 0 )
    {
      index.files.forEach((element) async => {
        dataStructure.addBook(await fetchBook(element))
      });
    }
    return dataStructure;
  }





}