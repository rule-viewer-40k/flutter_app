import 'package:rule_viewer_40k/models/index.dart';
import 'package:rule_viewer_40k/models/data-structure.dart';
import 'api-provider.dart';

class Repository {

  final String _gitUrl;
  ApiProvider _apiProvider;

  Repository(this._gitUrl) {
    _apiProvider = ApiProvider(_gitUrl);
  }

  Future<DataStructure> fetchData() => _apiProvider.fetchData();
}