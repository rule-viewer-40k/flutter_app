class Book {

  final String _error;

  Book(this._error);

  Book.fromJson(Map<String, dynamic> json)
      : _error = "";

  Book.withError(String errorValue)
      : _error = errorValue;

  String get error => _error;

}