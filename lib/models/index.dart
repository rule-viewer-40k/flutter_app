class Index {

  final String _title;
  final String _error;
  final List<String> _files;

  Index(this._title, this._files, this._error);


  Index.fromJson(Map<String, dynamic> json)
      : _error = "",
        _title = json['title'],
        _files = new List<String>.from(json['files']);

  Index.withError(String errorValue)
      : _error = errorValue,
        _title = "",
        _files = [];


  String get title => _title;
  List<String> get files => _files;
  String get error => _error;
}