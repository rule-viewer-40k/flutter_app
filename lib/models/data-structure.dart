import 'package:rule_viewer_40k/models/index.dart';
import 'package:rule_viewer_40k/models/book.dart';

class DataStructure {

  List<Book> _books = [];

  addBook(Book book)
  {
    this._books.add(book);
  }

  List<Book> get rules => _books;
}