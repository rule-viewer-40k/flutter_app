import 'package:flutter/material.dart';

class MainThemeLight {

  static ThemeData mainTheme = ThemeData(
      brightness:     Brightness.light,
      primarySwatch:  Colors.blue,
      fontFamily:     'Montserrat-Medium',
      textTheme:      ThemeData.light().textTheme
  );

}