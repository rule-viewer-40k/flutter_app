import 'package:flutter/material.dart';

class MainThemeDark {

  static ThemeData mainTheme = ThemeData(
      brightness:     Brightness.dark,
      primarySwatch:  Colors.blue,
      fontFamily:     'Montserrat-Medium',
      textTheme:      ThemeData.dark().textTheme
  );
}