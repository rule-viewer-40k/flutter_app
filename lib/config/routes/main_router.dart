import 'package:flutter/material.dart';
import 'package:fluro/fluro.dart';

import 'package:rule_viewer_40k/ui/Dashboard/dashboard_screen.dart';

class MainRouter {

  static FluroRouter router = FluroRouter();

  static Handler _dashboardHandler = Handler(
      handlerFunc: (
          BuildContext context,
          Map<String, dynamic> params
          ) => DashboardScreen()
  );

  static void setupRouter() {
    router.define(
      'dashboard',
      handler: _dashboardHandler,
    );
  }
}