import 'package:flutter/material.dart';
import 'package:rule_viewer_40k/config/routes/main_router.dart';
import 'package:rule_viewer_40k/config/themes/main_theme_light.dart';
import 'package:rule_viewer_40k/config/themes/main_theme_dark.dart';

void main() {
  MainRouter.setupRouter();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: MainThemeDark.mainTheme,
        initialRoute: 'dashboard',
        onGenerateRoute: MainRouter.router.generator
    );
  }
}
