import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rule_viewer_40k/widgets/slivers/sliver_header.dart';
import 'package:rule_viewer_40k/widgets/test/data-viewer.dart';

class DashboardScreen extends StatefulWidget {
  DashboardScreen({Key key}) : super(key: key);

  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {

  final TextEditingController _filter = new TextEditingController();

  List<String> itemList = [];

  @override
  void initState() {
    super.initState();
    for (int count = 0; count < 50; count++) {
      itemList.add("Item $count");
    }
  }

  @override
  Widget build(BuildContext context) {



    return Scaffold(
        body: CustomScrollView(
            physics: const BouncingScrollPhysics(),
            slivers: [
              SliverAppBar(


                floating: true,
                flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,
                  title: Text('Rule viewer'),
                  background : ShaderMask(
                    shaderCallback: (rect) {
                      return LinearGradient(
                        begin: Alignment(0.0, 0.8),
                        end: Alignment.bottomCenter,
                        colors: [Colors.black, Colors.transparent],
                      ).createShader(Rect.fromLTRB(0, 0, rect.width, rect.height));
                    },
                    blendMode: BlendMode.dstIn,
                    child: Image.asset(
                      'assets/images/banner.jpg',
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                // Make the initial height of the SliverAppBar larger than normal.
                expandedHeight: 70,
              ),
              SliverAppBar(
                  pinned: true,
                  flexibleSpace: SliverHeader()
              ),
              SliverToBoxAdapter(child: DataViewer())
            ]
        )
    );
  }

}