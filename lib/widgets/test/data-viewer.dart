import 'package:flutter/material.dart';
import 'package:rule_viewer_40k/blocs/data_structure/data_structure_bloc.dart';
import 'package:rule_viewer_40k/models/data-structure.dart';

class DataViewer extends StatefulWidget {DataViewer({Key key}) : super(key: key);

@override
_DataViewerState createState() => _DataViewerState();

}

class _DataViewerState extends State<DataViewer> {


  @override
  Widget build(BuildContext context) {
    dataStructureBloc..getData();
    return StreamBuilder<DataStructure>(
        stream: dataStructureBloc.subject.stream,
        builder: (context, AsyncSnapshot<DataStructure> snapshot) {
          if (snapshot.hasData) {
            return Text("file");
          } else if (snapshot.hasError) {
            return Text("error");
          } else {
            return Text("Loading");
          }
        });
  }
}