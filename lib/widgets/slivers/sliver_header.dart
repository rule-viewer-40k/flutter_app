import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SliverHeader extends StatefulWidget {
  SliverHeader({Key key}) : super(key: key);

  @override
  _SliverHeaderState createState() => _SliverHeaderState();
}

class _SliverHeaderState extends State<SliverHeader> {

  final TextEditingController _filter = new TextEditingController();


  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Card(
        elevation: 10,
        child: SizedBox(
          height: 40,
          width: MediaQuery.of(context).size.width*19.55/20,
          child: CupertinoTextField(
            controller: _filter,
            keyboardType: TextInputType.text,
            placeholder: 'Search',
            placeholderStyle: TextStyle(
              color: Color(0xffC4C6CC),
              fontSize: 14.0,
              fontFamily: 'Montserrat_Medium',
            ),
            prefix: Padding(
              padding: const EdgeInsets.fromLTRB(5.0, 5.0, 0.0, 5.0),
              child: Icon(
                Icons.search,
                size: 18,
                color: Colors.black,
              ),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0),
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}