import 'package:rule_viewer_40k/models/data-structure.dart';
import 'package:rule_viewer_40k/persistance/repository.dart';
import 'package:rxdart/rxdart.dart';

class DataStructureBloc {

  Repository repository = Repository("https://github.com/ThePyjaman/40k-Rules");

  final BehaviorSubject<DataStructure> _subject = BehaviorSubject<DataStructure>();

  getData() async {
    DataStructure response = await repository.fetchData();
    _subject.sink.add(response);
  }

  dispose() {
    _subject.close();
  }

  BehaviorSubject<DataStructure> get subject => _subject;

}

final dataStructureBloc = DataStructureBloc();
